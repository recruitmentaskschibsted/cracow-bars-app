# Cracow Bar's app

This app is not quite ready ;-( , but time is now, and i must stop all further works.
Application is starting on localhost on port 4200 and is looking for api on localhost on port 8080.
I wrote a couple of unit test, but coverage is unluckily very poor.
There is no any e2e tests :-(.
View of bar's detail is not ready :-(
Repo with api should be accessible [here](https://bitbucket.org/recruitmentaskschibsted/google-place-api-proxy)
I'm apologize for all nasty bugs, which you will find here


This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
