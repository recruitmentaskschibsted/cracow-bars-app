import { SchibappPage } from './app.po';

describe('schibapp App', function() {
  let page: SchibappPage;

  beforeEach(() => {
    page = new SchibappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
