import { Component, OnInit, AfterViewInit, OnDestroy, ComponentFactoryResolver, ViewChild } from '@angular/core';
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import "rxjs/add/operator/switchMap";
import "rxjs/add/observable/of";
import "rxjs/add/observable/merge";
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import Config from './app.config';
import { DataService, Bars, Bar } from './data.service';
import { LoaderHostDirective } from './loader-host.directive';
import { LoaderComponent } from './loader.component';

interface SearchTerms{
    str: string;
    nextpagetoken: string;
}

declare var $: any;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit, AfterViewInit, OnDestroy{
  bars: Observable<Bar[]>;
  private refilter = new Subject<SearchTerms>();
  spinner: boolean = false;
  nextpagetoken: string = '';
  str:string = '';
  pendingDrawn: string;
  @ViewChild(LoaderHostDirective) barHost: LoaderHostDirective;
  apikey: string = Config.apikey;

  constructor(private dataService: DataService,
    private _componentFactoryResolver: ComponentFactoryResolver){}

  ngOnInit(){

    this.bars = this.refilter
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap(filter => this.dataService.getList(filter.str, filter.nextpagetoken))
      .map(res=>{
        this.spinner = false;
        this.nextpagetoken = res.next_page_token || '';

        return (res && res.results && res.results.length)
          ? res.results : [];
      });

      setTimeout(()=>{
        this.doRefilter();
        this.handleScroll();
      },0);

  }

  ngAfterViewInit() {
        $(document).ready(() => {
            window.addEventListener('scroll', this.handleScroll.bind(this));
        });
  }

  ngOnDestroy() {
        window.removeEventListener('scroll', this.handleScroll);
  }

  onKeyupInput(str: string) {
    this.spinner = true;
    this.str = str;
    this.nextpagetoken = '';
    this.doRefilter();
  }

  doRefilter(){
    this.spinner = true;
    let filter:SearchTerms = {str: this.str, nextpagetoken: this.nextpagetoken};
    this.refilter.next(filter);
  }

  handleScroll(){

      if ($('#resultstable').offset().top+$('#resultstable').height()-200 < $('body').scrollTop() + window.innerHeight) {
          this.drawnMore();
      }
  }
  drawnMore(){

    if(!this.nextpagetoken){
      return;
    }
    if(this.pendingDrawn === this.nextpagetoken){
      return;
    }
    this.pendingDrawn = this.nextpagetoken;
    this.loadNext()
  }
  loadNext(){

    if(this.nextpagetoken){
      this.dataService
      .loadNext(this.nextpagetoken)
      .subscribe(res=>{
        console.log('res', res);

        let componentFactory = this._componentFactoryResolver.resolveComponentFactory(res.component);
        let viewContainerRef = this.barHost.viewContainerRef;
        //viewContainerRef.clear();
        let componentRef = viewContainerRef.createComponent(componentFactory);
        (<LoaderComponent>componentRef.instance).data = res.data;

      });
    }
  }
}
