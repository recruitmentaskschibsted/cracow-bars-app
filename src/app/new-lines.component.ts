import { Component, Input }  from '@angular/core';
import Config from './app.config';
import { LoaderComponent } from './loader.component';

@Component({
  template: `
  <li *ngFor="let bar of data" class="collection-item avatar large">
     <img *ngIf="bar.photos && bar.photos.length" src="https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference={{bar.photos[0].photo_reference}}&key={{apikey}}" alt="" class="circle large">
     <div class="title"><a [routerLink]="['bar', bar.place_id]">{{bar.name}}</a>
     <a [routerLink]="['bar', bar.place_id]" class="secondary-content"><i class="material-icons">send</i></a></div>
  </li>
  `
})
export class NewLinesComponent implements LoaderComponent {
  @Input() data: any;
  apikey: string = Config.apikey;
}
