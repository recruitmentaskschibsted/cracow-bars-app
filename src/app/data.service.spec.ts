import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { Http, Response, RequestOptions, BaseRequestOptions, ResponseOptions, ConnectionBackend } from '@angular/http';
import { DataService } from './data.service';

describe('DataService',()=>{

  let dataService:DataService, mockBackend:MockBackend, lastConnection;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: ConnectionBackend, useClass: MockBackend},
        {provide: RequestOptions, useClass: BaseRequestOptions},
        Http,
        DataService
      ]})
      dataService = TestBed.get(DataService);
      mockBackend =  TestBed.get(ConnectionBackend);
      mockBackend.connections.subscribe((connection: any) => lastConnection = connection);

    });

  it('proper methods ', inject([DataService], (service: DataService) => {
      expect(service.getList).toBeDefined();
      expect(service.getDetail).toBeDefined();
  }));

  it('http underneath should get well ', fakeAsync(() => {
    let result: any;

    dataService
      .getList('angel', 'sdsd')
      .subscribe((response) => {
        result = response;
      });
    expect(lastConnection.request.url).toBeDefined();
    expect(lastConnection.request.url).toMatch(/filter\=angel\&pagetoken\=sdsd/);

    lastConnection.mockRespond(new Response(new ResponseOptions({
      body: JSON.stringify({items: [{id: 1}, {id: 2}]}),
    })));
    tick();
    expect(result).toEqual({items: [{id: 1}, {id: 2}]})
  }));
  it('http error should be catched', fakeAsync(()=>{
    let result: any;

    dataService
      .getList('bar', 'zzzzz')
      .subscribe((response) => {
        result = response;
      });

    lastConnection.mockRespond(new Response(new ResponseOptions({
        status: 422,
        statusText: 'Unprocessable entity'
    })));
    tick();
    expect(result).toBeNull();
  }));
})
