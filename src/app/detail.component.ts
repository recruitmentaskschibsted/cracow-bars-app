import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { DataService, Bar } from './data.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html'
})

export class DetailComponent implements OnInit{
  barData: Bar
  constructor(
        private actRouter: ActivatedRoute,
        private dataService: DataService
    ) { }

  ngOnInit(){
     this.actRouter.params.switchMap(params => {
         return params['id'] ?
             this.dataService.getDetail(params['id']) : Observable.of({})
     })
     .subscribe(data=> this.barData = data);
  }

}
