/* tslint:disable:no-unused-variable */
import { DebugElement }    from '@angular/core';
import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { By } from '@angular/platform-browser';
import { RouterModule, RouterLink, RouterLinkWithHref, Router, ActivatedRoute } from '@angular/router';
import { LocationStrategy } from '@angular/common';
import {InternalMenuComponent} from './internal-menu.component';
import { RouterLinkStubDirective,
    RouterStub,
    ActivatedRouteStub,
    RouterOutletStubComponent,
    LocationStrategyStub } from '../testing/router-stubs';


import { LoaderHostDirective } from './loader-host.directive';
import { ListComponent } from './list.component';
import { DataService } from './data.service';

describe('ListComponent', () => {

  const dataServiceStub, fixture, app, serviceCalled, dataService;
  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [RouterModule],
      declarations: [
        ListComponent,
        LoaderHostDirective
      ],
      providers: [
        DataService,
        {provide: Http, useValue: jasmine.createSpyObj('http', ['get'])},
        {provide: RouterLink, useClass: RouterLinkStubDirective},
        {provide: RouterLinkWithHref, useClass: RouterLinkStubDirective},
        {provide: Router, useClass: RouterStub},
        {provide: ActivatedRoute, useClass: ActivatedRouteStub},
        {provide: LocationStrategy, useClass: LocationStrategyStub}
        ]
    }).compileComponents().then(function(){
      fixture = TestBed.createComponent(ListComponent);
      dataService = TestBed.get(DataService);
      app = fixture.debugElement.componentInstance;
      spyOn(app, 'loadNext');
      spyOn(window, 'setInterval');
      spyOn(dataService, 'getList').and.callFake(function(){
        return Observable.of<any>([])
      });
      spyOn(app.refilter,'debounceTime').and.callThrough();

    });

  }));
  it('proper methods should be defined', async(() => {

    expect(app.ngOnInit).toBeDefined();
    expect(app.onKeyupInput).toBeDefined();
    expect(app.doRefilter).toBeDefined();
    fixture.detectChanges();
    expect(app.bars).toBeDefined();
    expect(app.refilter).toBeDefined();
    expect(app.spinner).toBeDefined();
    expect(app.nextpagetoken).toBeDefined();
    expect(app.str).toBeDefined();

  }));
  it('should create the app', async(() => {
    expect(app).toBeTruthy();
  }));

  it('should have an input', async(() => {
    const de = fixture.debugElement.query(By.css('input'));
    const el = de.nativeElement;
    expect(el.id).toEqual('barsearch');
  }));

  it('input event should involve Observable', async(() => {
    const de = fixture.debugElement.query(By.css('input'));
    const el = de.nativeElement;
    fixture.detectChanges();
    spyOn(app,'onKeyupInput').and.callThrough();
    spyOn(app,'doRefilter');
    de.triggerEventHandler('keyup', 'test');
    expect(app.onKeyupInput).toHaveBeenCalled();
    expect(app.doRefilter).toHaveBeenCalled();
  }));

  xit('service should be called', fakeAsync(()=>{
    fixture.detectChanges();

    app.refilter.next({str: 'test', nextpagetoken: 'sdsds'});
    tick(500);
    expect(app.refilter.debounceTime).toHaveBeenCalled();
    expect(dataService.getList).toHaveBeenCalled();
    expect(dataService.getList).toHaveBeenCalledWith('test', 'sdsds');

  }));

});
