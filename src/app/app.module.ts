import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, PreloadAllModules } from '@angular/router';

import { AppComponent } from './app.component';
import { ListComponent } from './list.component';
import { DetailComponent } from './detail.component';
import { LoaderHostDirective } from './loader-host.directive';
import { NewLinesComponent } from './new-lines.component';
import { DataService } from './data.service';
import { ROUTES } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    DetailComponent,
    LoaderHostDirective,
    NewLinesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES, { preloadingStrategy: PreloadAllModules})
  ],
  providers: [ DataService ],
  bootstrap: [ AppComponent ],
  entryComponents: [ NewLinesComponent ],
})
export class AppModule { }
