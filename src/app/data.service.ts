import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {Observable} from 'rxjs/Observable';
import Config from './app.config';

import { LoadItem } from './load-item';
import { NewLinesComponent } from './new-lines.component';

export interface Bar{
  geometry: any;
  icon: string;
  id: string;
  name: string;
  opening_hours:any;
  photos: any[];
  place_id: string;
  rating: number;
  reference: string;
  scope: string;
  types: any[];
  vicinity: string;
}

export interface Bars{
  html_attributions: any[];
  results: Bar[];
  next_page_token: string;
  status: string;
}

@Injectable()
export class DataService {

    constructor(private http: Http) {}

    getList(filter: string = '', pagetoken:string = ''): Observable<Bars>{

      const headers = new Headers({
         'Accept': 'application/json'
     })
     const options = new RequestOptions({headers: headers});
     const querystring = `?filter=${encodeURIComponent(filter)}&pagetoken=${pagetoken}`;

      return this.http.get(Config.api_host+'/places'+querystring, options)
        .map(res => res.json())
        .catch(err=>Observable.of(null));
    }
    getDetail(placeid: string): Observable<Bar>{
      const headers = new Headers({
         'Accept': 'application/json'
     })
     const options = new RequestOptions({headers: headers});

      return this.http.get(Config.api_host+'/places/'+placeid, options)
        .map(res => res.json())
        .catch(err=>Observable.of(null));
    }
    loadNext(nexttoken: string): Observable<LoadItem>{
      const headers = new Headers({
        'Accept': 'application/json'
      })
      const options = new RequestOptions({headers: headers});
      const querystring = `?filter=pagetoken=${nexttoken}`;

      return this.http.get(Config.api_host+'/places'+querystring, options)
        .map(res => res.json())
        .map(res=>(res && res.results && res.results.length)
          ? res.results : [])
        .map(results=>new LoadItem(NewLinesComponent, results))
        .catch(err=>Observable.of(null));
        
    }
}
