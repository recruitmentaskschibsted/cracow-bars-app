import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ListComponent } from './list.component';
import { DetailComponent } from './detail.component';


export const ROUTES: Routes = [
    {
        path: '',
        component: AppComponent,
        children: [
          {
            path: '',
            component: ListComponent
          },
          {
            path: 'bar/:id',
            component: DetailComponent
          }
      ]
    }
];
