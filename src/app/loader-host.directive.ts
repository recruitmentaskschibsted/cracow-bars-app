import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: 'loader-host',
})
export class LoaderHostDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
