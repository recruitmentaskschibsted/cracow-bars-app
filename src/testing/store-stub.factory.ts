import { StoreStub, Dispatcher } from "./store-stub";
import { NgRXreducers } from '../shared/lib/ngrx/reducers';

const combineReducers = reducers => (state = {}, action) => {
    return Object.keys(reducers).reduce((nextState, key) => {
        nextState[key] = reducers[key](state[key], action);
        return nextState;
    }, {});
};

export class StoreStubFactory{
    static factory(dispatcher:any = null, reducer:any = null, preMiddleware:any = null, postMiddleware:any = null, initialState:any = {}){
        if(!dispatcher){
            dispatcher = new Dispatcher()
        }
        if(!reducer){
            reducer = combineReducers(NgRXreducers);
        }
        if(!preMiddleware){
            preMiddleware = obs => { return obs.do(val => {} /*console.log('ACTION:', val)*/)};
            //preMiddleware = () => {};
        }
        if(!postMiddleware){
            postMiddleware = obs => { return obs.do(val => {}/*console.log('STATE:', val)*/)};
            //postMiddleware = () => {};
        }

        return new StoreStub(dispatcher, reducer, preMiddleware, postMiddleware, initialState)
    }
}