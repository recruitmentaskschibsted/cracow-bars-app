// export for convenience.
export {ActivatedRoute, Router, RouterLink, RouterOutlet, Params} from '@angular/router';

import {Component, Directive, Injectable, Input} from '@angular/core';
import {NavigationExtras} from '@angular/router';

@Directive({
    selector: '[routerLink]',
    host: {
        '(click)': 'onClick()'
    }
})
export class RouterLinkStubDirective {
    @Input('routerLink') linkParams: any;
    navigatedTo: any = null;
    subscription: any = {
        unsubscribe: function () {
        }
    };

    onClick() {
        this.navigatedTo = this.linkParams;
    }
    unsubscribe = function () {
    }
}

@Component({selector: 'router-outlet', template: ''})
export class RouterOutletStubComponent {
}

@Injectable()
export class RouterStub {
    navigate(commands: any[], extras?: NavigationExtras) {
    }

    events = {
        subscribe: function () {
            return this;
        },
        unsubscribe: function () {
        }
    };
    createUrlTree = function () {
    };
    serializeUrl = function () {
    };
}


// Only implements params and part of snapshot.params
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class ActivatedRouteStub {

    // ActivatedRoute.params is Observable
    private _testParams: {} = {};
    private subject = new BehaviorSubject(this.testParams);
    //private subject = new Subject();
    params = this.subject.asObservable();

    // Test parameters

    get testParams() {
        return this._testParams;
    }

    set testParams(params: {}) {
        console.log("params", params);
        this._testParams = params;
        this.subject.next(params);
    }

    // ActivatedRoute.snapshot.params
    get snapshot() {
        return {params: this.testParams};
    }
}

@Injectable()
export class LocationStrategyStub {
    prepareExternalUrl = function(){};
}


/*
 Copyright 2016 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that
 can be found in the LICENSE file at http://angular.io/license
 */