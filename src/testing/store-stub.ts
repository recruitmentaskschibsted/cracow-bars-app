import {Subject} from 'rxjs/Subject';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

export class Dispatcher extends Subject<any>{
    dispatch(value : any) : void {
        this.next(value);
    }
}

export class StoreStub extends BehaviorSubject<any>{
    constructor(
        private dispatcher,
        private reducer,
        preMiddleware,
        postMiddleware,
        initialState = {}
    ){
        super(initialState);
        this.dispatcher
            .let(preMiddleware)
            .scan((state, action) => this.reducer(state, action), initialState)
            .let(postMiddleware)
            .subscribe(state => super.next(state));
    }

    //distinctUntilChanged only emits new values when output is distinct, per last emitted   //value. In the example below, the observable with the distinctUntilChanged operator
    //will emit one less value then the other with only the map operator
    select(key : string) {

        return this
            .map(state => state[key])
            .distinctUntilChanged();
    }

    dispatch(value){
        this.dispatcher.dispatch(value);
    }

    next(value){
        this.dispatcher.dispatch(value);
    }
}